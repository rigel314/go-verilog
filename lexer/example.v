/*-----------------------------------------------------
 * Design Name : muxmux
 * File Name   : example.v
 * Function    : 2:1 Mux
 * Coder       : name here
 *-----------------------------------------------------*/
module mux (
    input  din_0,  // Mux first input
    input  din_1,  // Mux Second input
    input  sel,    // Select input
    output mux_out // Mux output
);

//------------Internal Variables--------
wire mux_out;
//-------------Code Start-----------------
assign mux_out = (sel) ? din_1 : din_0;

reg [7:0] thing;
initial begin
    $display("testing...");
    thing = 8'hFF;
end

endmodule // End Of Module mux
