package lexer

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"unicode"

	"gitlab.com/rigel314/go-verilog/token"
)

// Lexer scans tokens from a verilog source
type Lexer struct {
	r         *bufio.Reader
	fname     string
	pos       Pos
	prevpos   Pos
	prev2pos  Pos
	prevrune  rune
	prev2rune rune
	readPrev2 bool
}

// NewLexer wraps an io.Reader, which should contain verilog source
func NewLexer(filename string, r io.Reader) *Lexer {
	return &Lexer{
		r:         bufio.NewReader(r),
		fname:     filename,
		pos:       Pos{Line: 1, OffsetInLine: 1},
		prevrune:  eofrune,
		prev2rune: eofrune,
	}
}

const eofrune = rune(-1)

func (l *Lexer) nextrune() rune {
	l.prevpos, l.prev2pos = l.pos, l.prevpos
	var char rune
	if l.readPrev2 {
		l.readPrev2 = false
		char = l.prev2rune
		l.prev2rune = eofrune
		l.prevrune = eofrune
	} else {
		var err error
		char, _, err = l.r.ReadRune()
		if err != nil {
			return eofrune
		}
	}
	l.pos.OffsetInLine++
	if char == '\n' {
		l.pos.Line++
		l.pos.OffsetInLine = 1
	}
	l.prevrune, l.prev2rune = char, l.prevrune
	return char
}

func (l *Lexer) unreadrune() {
	err := l.r.UnreadRune() // can only fail if a rune was not previously read
	l.pos, l.prevpos = l.prevpos, l.prev2pos
	if err != nil && (l.prev2rune == eofrune || l.readPrev2) {
		panic("too many unreads")
	}
	if err != nil && l.prev2rune != eofrune {
		l.readPrev2 = true
	}
}

// Lex scans the next token available to the Lexer
func (l *Lexer) Lex() *Token {
	char := l.nextrune()

	p := l.pos

	switch true {
	case unicode.IsSpace(char):
		l.unreadrune()
		return l.scanWhitspace()
	case isDecimalNumber(char):
		l.unreadrune()
		return l.scanNumber()
	case isIdentInitial(char):
		l.unreadrune()
		return l.scanIdentifier()
	}

	switch char {
	case eofrune:
		return &Token{Typ: token.EOF, Filename: l.fname, Pos: p}

	// String Constant
	case '"':
		l.unreadrune()
		return l.scanString()

	case '\'':
		char2 := l.nextrune()
		switch char2 {
		case 's', 'S':
			char3 := l.nextrune()
			switch char3 {
			case 'h', 'd', 'o', 'b':
				return &Token{Typ: token.TICK, Lit: "'" + string(char2) + string(char3), Filename: l.fname, Pos: p}
			default:
				l.unreadrune()
				return &Token{Typ: token.ILLEGAL, Lit: "'" + string(char2), Filename: l.fname, Pos: p}
			}
		case 'h', 'd', 'o', 'b':
			return &Token{Typ: token.TICK, Lit: "'" + string(char2), Filename: l.fname, Pos: p}
		case '0', '1', 'x', 'X', 'z', 'Z', '?':
			return &Token{Typ: token.UNBASEDUNSIZED, Lit: "'" + string(char2), Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.TICK, Lit: "'", Filename: l.fname, Pos: p}
		}

	// Operators
	case '/':
		char2 := l.nextrune()
		switch char2 {
		case '/':
			return l.scanSingleComment()
		case '*':
			return l.scanMultiComment()
		case '=':
			return &Token{Typ: token.DIV_ASSIGN, Lit: "/=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.DIV, Lit: "/", Filename: l.fname, Pos: p}
		}
	case '=':
		char2 := l.nextrune()
		switch char2 {
		case '=':
			char3 := l.nextrune()
			switch char3 {
			case '=':
				return &Token{Typ: token.CASE_EQUAL, Lit: "===", Filename: l.fname, Pos: p}
			case '?':
				return &Token{Typ: token.WILD_EQUAL, Lit: "==?", Filename: l.fname, Pos: p}
			default:
				l.unreadrune()
				fallthrough
			case eofrune:
				return &Token{Typ: token.EQUAL, Lit: "==", Filename: l.fname, Pos: p}
			}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.ASSIGN, Lit: "=", Filename: l.fname, Pos: p}
		}
	case '!':
		char2 := l.nextrune()
		switch char2 {
		case '=':
			char3 := l.nextrune()
			switch char3 {
			case '=':
				return &Token{Typ: token.CASE_NOT_EQUAL, Lit: "!==", Filename: l.fname, Pos: p}
			case '?':
				return &Token{Typ: token.WILD_NOT_EQUAL, Lit: "!=?", Filename: l.fname, Pos: p}
			default:
				l.unreadrune()
				fallthrough
			case eofrune:
				return &Token{Typ: token.NOT_EQUAL, Lit: "!=", Filename: l.fname, Pos: p}
			}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.LOGICAL_NOT, Lit: "!", Filename: l.fname, Pos: p}
		}
	case '-':
		char2 := l.nextrune()
		switch char2 {
		case '-':
			return &Token{Typ: token.DEC, Lit: "--", Filename: l.fname, Pos: p}
		case '=':
			return &Token{Typ: token.SUB_ASSIGN, Lit: "-=", Filename: l.fname, Pos: p}
		case '>':
			return &Token{Typ: token.IMP, Lit: "->", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.SUB, Lit: "-", Filename: l.fname, Pos: p}
		}
	case '+':
		char2 := l.nextrune()
		switch char2 {
		case '+':
			return &Token{Typ: token.INC, Lit: "++", Filename: l.fname, Pos: p}
		case '=':
			return &Token{Typ: token.ADD_ASSIGN, Lit: "+=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.ADD, Lit: "+", Filename: l.fname, Pos: p}
		}
	case '*':
		char2 := l.nextrune()
		switch char2 {
		case '*':
			return &Token{Typ: token.POW, Lit: "**", Filename: l.fname, Pos: p}
		case '=':
			return &Token{Typ: token.MUL_ASSIGN, Lit: "*=", Filename: l.fname, Pos: p}
		case ')':
			return &Token{Typ: token.RATTR, Lit: "*)", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.MUL, Lit: "*", Filename: l.fname, Pos: p}
		}
	case '?':
		return &Token{Typ: token.QUESTION, Lit: "?", Filename: l.fname, Pos: p}
	case ':':
		char2 := l.nextrune()
		switch char2 {
		case ':':
			return &Token{Typ: token.SCOPE, Lit: "::", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.COLON, Lit: ":", Filename: l.fname, Pos: p}
		}
	case '&':
		char2 := l.nextrune()
		switch char2 {
		case '&':
			return &Token{Typ: token.LOGICAL_AND, Lit: "&&", Filename: l.fname, Pos: p}
		case '=':
			return &Token{Typ: token.AND_ASSIGN, Lit: "&=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.AND, Lit: "&", Filename: l.fname, Pos: p}
		}
	case '|':
		char2 := l.nextrune()
		switch char2 {
		case '|':
			return &Token{Typ: token.LOGICAL_OR, Lit: "||", Filename: l.fname, Pos: p}
		case '=':
			return &Token{Typ: token.OR_ASSIGN, Lit: "|=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.OR, Lit: "|", Filename: l.fname, Pos: p}
		}
	case '%':
		char2 := l.nextrune()
		switch char2 {
		case '=':
			return &Token{Typ: token.REM_ASSIGN, Lit: "%=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.REM, Lit: "%", Filename: l.fname, Pos: p}
		}
	case '^':
		char2 := l.nextrune()
		switch char2 {
		case '~':
			return &Token{Typ: token.NOT_XOR, Lit: "^~", Filename: l.fname, Pos: p}
		case '=':
			return &Token{Typ: token.XOR_ASSIGN, Lit: "^=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.XOR, Lit: "^", Filename: l.fname, Pos: p}
		}
	case '~':
		char2 := l.nextrune()
		switch char2 {
		case '&':
			return &Token{Typ: token.NAND_REDUCE, Lit: "~&", Filename: l.fname, Pos: p}
		case '|':
			return &Token{Typ: token.NOR_REDUCE, Lit: "~|", Filename: l.fname, Pos: p}
		case '^':
			return &Token{Typ: token.XNOR_REDUCE, Lit: "~^", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.NOT, Lit: "~", Filename: l.fname, Pos: p}
		}
	case '<':
		char2 := l.nextrune()
		switch char2 {
		case '<':
			char3 := l.nextrune()
			switch char3 {
			case '<':
				char4 := l.nextrune()
				switch char4 {
				case '=':
					return &Token{Typ: token.SHIFT_LEFT_ASSIGN, Lit: "<<<=", Filename: l.fname, Pos: p}
				default:
					l.unreadrune()
					fallthrough
				case eofrune:
					return &Token{Typ: token.SHIFT_LEFT, Lit: "<<<", Filename: l.fname, Pos: p}
				}
			case '=':
				return &Token{Typ: token.LOGICAL_SHIFT_LEFT_ASSIGN, Lit: "<<=", Filename: l.fname, Pos: p}
			default:
				l.unreadrune()
				fallthrough
			case eofrune:
				return &Token{Typ: token.LOGICAL_SHIFT_LEFT, Lit: "<<", Filename: l.fname, Pos: p}
			}
		case '-':
			char3 := l.nextrune()
			switch char3 {
			case '>':
				return &Token{Typ: token.EQUIVALENCE, Lit: "<->", Filename: l.fname, Pos: p}
			default:
				l.unreadrune()
				fallthrough
			case eofrune:
				l.unreadrune() // l has special support for exactly 2 unreads after 2 successful reads
				return &Token{Typ: token.LESS, Lit: "<", Filename: l.fname, Pos: p}
			}
		case '=':
			return &Token{Typ: token.LESS_EQUAL, Lit: "<=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.LESS, Lit: "<", Filename: l.fname, Pos: p}
		}
	case '>':
		char2 := l.nextrune()
		switch char2 {
		case '>':
			char3 := l.nextrune()
			switch char3 {
			case '>':
				char4 := l.nextrune()
				switch char4 {
				case '=':
					return &Token{Typ: token.SHIFT_RIGHT_ASSIGN, Lit: ">>>=", Filename: l.fname, Pos: p}
				default:
					l.unreadrune()
					fallthrough
				case eofrune:
					return &Token{Typ: token.SHIFT_RIGHT, Lit: ">>>", Filename: l.fname, Pos: p}
				}
			case '=':
				return &Token{Typ: token.LOGICAL_SHIFT_RIGHT_ASSIGN, Lit: ">>=", Filename: l.fname, Pos: p}
			default:
				l.unreadrune()
				fallthrough
			case eofrune:
				return &Token{Typ: token.LOGICAL_SHIFT_RIGHT, Lit: ">>", Filename: l.fname, Pos: p}
			}
		case '=':
			return &Token{Typ: token.GREATER_EQUAL, Lit: ">=", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.GREATER, Lit: ">", Filename: l.fname, Pos: p}
		}

		// Symbols
	case '(':
		char2 := l.nextrune()
		switch char2 {
		case '*':
			return &Token{Typ: token.LATTR, Lit: "(*", Filename: l.fname, Pos: p}
		default:
			l.unreadrune()
			fallthrough
		case eofrune:
			return &Token{Typ: token.LPAREN, Lit: "(", Filename: l.fname, Pos: p}
		}
	case ')':
		return &Token{Typ: token.RPAREN, Lit: ")", Filename: l.fname, Pos: p}

	case '[':
		return &Token{Typ: token.LBRACKET, Lit: "[", Filename: l.fname, Pos: p}
	case ']':
		return &Token{Typ: token.RBRACKET, Lit: "]", Filename: l.fname, Pos: p}
	case '{':
		return &Token{Typ: token.LBRACE, Lit: "{", Filename: l.fname, Pos: p}
	case '}':
		return &Token{Typ: token.RBRACE, Lit: "}", Filename: l.fname, Pos: p}
	case ';':
		return &Token{Typ: token.SEMICOLON, Lit: ";", Filename: l.fname, Pos: p}
	case ',':
		return &Token{Typ: token.COMMA, Lit: ",", Filename: l.fname, Pos: p}
	case '.':
		return &Token{Typ: token.PERIOD, Lit: ".", Filename: l.fname, Pos: p}
	case '#':
		return &Token{Typ: token.HASH, Lit: "#", Filename: l.fname, Pos: p}
	}

	return &Token{Typ: token.ILLEGAL, Filename: l.fname, Pos: p}
}

func (l *Lexer) scanWhitspace() *Token {
	p := l.pos
	var buf bytes.Buffer
	for {
		if char := l.nextrune(); char == eofrune {
			break
		} else if !unicode.IsSpace(char) {
			l.unreadrune()
			break
		} else {
			buf.WriteRune(char)
		}
	}

	return &Token{Typ: token.WHITESPACE, Lit: buf.String(), Filename: l.fname, Pos: p}
}

func (l *Lexer) scanSingleComment() *Token {
	p := l.pos
	p.OffsetInLine -= 2
	var buf bytes.Buffer
	buf.WriteString("//")
	for {
		if char := l.nextrune(); char == eofrune {
			break
		} else if char == '\n' {
			l.unreadrune()
			break
		} else {
			buf.WriteRune(char)
		}
	}

	return &Token{Typ: token.COMMENT, Lit: buf.String(), Filename: l.fname, Pos: p}
}

func (l *Lexer) scanMultiComment() *Token {
	p := l.pos
	p.OffsetInLine -= 2
	var buf bytes.Buffer
	buf.WriteString("/*")
	prevchar := eofrune
	for {
		char := l.nextrune()
		if char == eofrune {
			break
		} else if char == '/' && prevchar == '*' {
			buf.WriteRune(char)
			break
		} else {
			buf.WriteRune(char)
		}
		prevchar = char
	}

	return &Token{Typ: token.COMMENT, Lit: buf.String(), Filename: l.fname, Pos: p}
}

func (l *Lexer) scanString() *Token {
	p := l.pos
	var buf bytes.Buffer
	esc := true // initialize true so we don't stop on the first quote
	for {
		char := l.nextrune()
		if char == eofrune {
			break
		} else if char == '"' && !esc {
			buf.WriteRune(char)
			break
		} else {
			buf.WriteRune(char)
		}
		if char == '\\' && !esc {
			esc = true
		} else {
			esc = false
		}
	}

	return &Token{Typ: token.STRING, Lit: buf.String(), Filename: l.fname, Pos: p}
}

func (l *Lexer) scanNumber() *Token {
	p := l.pos
	var buf bytes.Buffer
	for {
		char := l.nextrune()
		if char == eofrune {
			break
		} else if !isNumber(char, true) {
			l.unreadrune()
			break
		} else {
			buf.WriteRune(char)
		}
	}

	return &Token{Typ: token.NUMBER, Lit: buf.String(), Filename: l.fname, Pos: p}
}

func (l *Lexer) scanIdentifier() *Token {
	p := l.pos
	var buf bytes.Buffer
	firstchar := eofrune
	for {
		char := l.nextrune()
		if firstchar == eofrune {
			firstchar = char
		}
		if char == eofrune {
			break
		} else if firstchar == '\\' && unicode.IsSpace(char) {
			l.unreadrune()
			break
		} else if firstchar == '`' && !isIdent(char) {
			if buf.Len() > 0 {
				l.unreadrune()
				break
			} else {
				buf.WriteRune(char)
			}
		} else if !isIdent(char) {
			l.unreadrune()
			break
		} else {
			buf.WriteRune(char)
		}
	}

	typ := token.Lookup(buf.String())

	if firstchar == '`' {
		typ = token.MACRO
	}

	return &Token{Typ: typ, Lit: buf.String(), Filename: l.fname, Pos: p}
}

// Pos hold line/offset information for a token
type Pos struct {
	Line         int
	OffsetInLine int
}

// Token holds one token as lexed
type Token struct {
	Filename string
	Typ      token.Token
	Lit      string
	Pos      Pos
}

func (t Token) String() (s string) {
	s = fmt.Sprintf("%s:%d:%d %v %q", t.Filename, t.Pos.Line, t.Pos.OffsetInLine, t.Typ, t.Lit)
	return
}

func isNumber(r rune, includeUnderscore bool) bool {
	if ('0' <= r && r <= '9') ||
		('a' <= r && r <= 'f') ||
		('A' <= r && r <= 'F') {
		return true
	}
	switch r {
	case 'x', 'X', 'z', 'Z', '?':
		return true
	}
	if includeUnderscore && r == '_' {
		return true
	}
	return false
}

func isDecimalNumber(r rune) bool {
	if '0' <= r && r <= '9' {
		return true
	}
	return false
}

func isIdentInitial(r rune) bool {
	if r == '`' || r == '$' || r == '\\' || r == '_' ||
		('a' <= r && r <= 'z') ||
		('A' <= r && r <= 'Z') {
		return true
	}
	return false
}

func isIdent(r rune) bool {
	if r == '$' || r == '_' ||
		('a' <= r && r <= 'z') ||
		('A' <= r && r <= 'Z') ||
		('0' <= r && r <= '9') {
		return true
	}
	return false
}
