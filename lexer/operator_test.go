package lexer_test

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/rigel314/go-verilog/lexer"
	"gitlab.com/rigel314/go-verilog/token"
)

func TestNominalOperators(t *testing.T) {
	ops := []string{
		"/", "/=",
		"=", "==", "===", "==?",
		"!", "!=", "!==", "!=?",
		"-", "--", "-=", "->",
		"+", "++", "+=",
		"*", "**", "*=", "*)",
		"?",
		":", "::",
		"&", "&&", "&=",
		"|", "||", "|=",
		"%", "%=",
		"^", "^~", "^=",
		"~", "~&", "~|", "~^",
		"<", "<=", "<<", "<<=", "<<<", "<<<=", "<->",
		">", ">=", ">>", ">>=", ">>>", ">>>=",
		"(", "(*", ")", "[", "]", "{", "}", ";", ",", ".", "#",
	}
	tks := []token.Token{token.DIV, token.DIV_ASSIGN,
		token.ASSIGN, token.EQUAL, token.CASE_EQUAL, token.WILD_EQUAL,
		token.LOGICAL_NOT, token.NOT_EQUAL, token.CASE_NOT_EQUAL, token.WILD_NOT_EQUAL,
		token.SUB, token.DEC, token.SUB_ASSIGN, token.IMP,
		token.ADD, token.INC, token.ADD_ASSIGN,
		token.MUL, token.POW, token.MUL_ASSIGN, token.RATTR,
		token.QUESTION,
		token.COLON, token.SCOPE,
		token.AND, token.LOGICAL_AND, token.AND_ASSIGN,
		token.OR, token.LOGICAL_OR, token.OR_ASSIGN,
		token.REM, token.REM_ASSIGN,
		token.XOR, token.NOT_XOR, token.XOR_ASSIGN,
		token.NOT, token.NAND_REDUCE, token.NOR_REDUCE, token.XNOR_REDUCE,
		token.LESS, token.LESS_EQUAL, token.LOGICAL_SHIFT_LEFT, token.LOGICAL_SHIFT_LEFT_ASSIGN, token.SHIFT_LEFT, token.SHIFT_LEFT_ASSIGN, token.EQUIVALENCE,
		token.GREATER, token.GREATER_EQUAL, token.LOGICAL_SHIFT_RIGHT, token.LOGICAL_SHIFT_RIGHT_ASSIGN, token.SHIFT_RIGHT, token.SHIFT_RIGHT_ASSIGN,
		token.LPAREN, token.LATTR, token.RPAREN, token.LBRACKET, token.RBRACKET, token.LBRACE, token.RBRACE, token.SEMICOLON, token.COMMA, token.PERIOD, token.HASH}
	for i, v := range ops {
		t.Log("trying", v)
		l := lexer.NewLexer(os.Stdin.Name(), strings.NewReader(v))
		tk := l.Lex()
		if tk.Typ != tks[i] || tk.Lit != v {
			t.Logf("expected {Typ: %v, Lit: %v}, got {Typ: %v, Lit: %v}", tks[i], v, tk.Typ, tk.String())
			t.Fail()
		}
	}
}

func TestInvalidLogicalEquivalence(t *testing.T) {
	l := lexer.NewLexer(os.Stdin.Name(), strings.NewReader("<--"))
	tk := l.Lex()
	if tk.Typ != token.LESS || tk.Lit != "<" {
		t.Logf("expected {Typ: %v, Lit: %v}, got {Typ: %v, Lit: %v}", token.LESS, "<", tk.Typ, tk.String())
		t.Fail()
	}
	tk = l.Lex()
	if tk.Typ != token.DEC || tk.Lit != "--" {
		t.Logf("expected {Typ: %v, Lit: %v}, got {Typ: %v, Lit: %v}", token.DEC, "--", tk.Typ, tk.String())
		t.Fail()
	}
}
