verilogfmt:
	GOOS=linux   GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.linux.amd64       ./cmd/verilogfmt
	GOOS=linux   GOARCH=386   go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.linux.386         ./cmd/verilogfmt
	GOOS=linux   GOARCH=arm64 go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.linux.arm64       ./cmd/verilogfmt
	GOOS=linux   GOARCH=arm   go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.linux.arm         ./cmd/verilogfmt
	GOOS=windows GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.windows.amd64.exe ./cmd/verilogfmt
	GOOS=windows GOARCH=386   go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.windows.386.exe   ./cmd/verilogfmt
	GOOS=darwin  GOARCH=amd64 go build -ldflags '-buildid= -w -s' -trimpath -o verilogfmt.darwin.amd64      ./cmd/verilogfmt

.PHONY: verilogfmt
