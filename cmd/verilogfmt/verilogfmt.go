package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/rigel314/go-verilog/lexer"
	"gitlab.com/rigel314/go-verilog/parser"
	"gitlab.com/rigel314/go-verilog/token"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()

	var in io.Reader
	if flag.Arg(0) != "" {
		f, err := os.Open(flag.Arg(0))
		if err != nil {
			log.Fatal("open error:", err)
		}
		defer f.Close()
		in = f
	} else {
		in = os.Stdin
	}

	lx := lexer.NewLexer(flag.Arg(0), in)

	inv := 0
	for {
		tk := lx.Lex()
		if tk.Typ == token.ILLEGAL {
			inv++
			// continue
		}
		fmt.Println(tk)
		if tk.Typ == token.EOF {
			break
		}
	}
	// fmt.Println(lexer.Lex(in))
	fmt.Println(inv, "invalid tokens")

	p := parser.NewParser("", bytes.NewBuffer(([]byte)("timeunit 1ns/1ps;")))

	parse, err := p.ParseSV()
	if err != nil {
		log.Fatal(err)
	}
	_ = parse.String()
	log.Println(err, "\n", parse)
}
