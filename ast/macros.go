package ast

import (
	"gitlab.com/rigel314/go-verilog/lexer"
)

type Macro struct {
	Doink  lexer.Pos      // position of "`" starting the macro
	Tokens []*lexer.Token // list of tokens that belong to the macro
}

func (m *Macro) Pos() lexer.Pos { return m.Doink }
func (m *Macro) End() lexer.Pos {
	return m.Tokens[len(m.Tokens)-1].Pos
}

func newMacro(t *lexer.Token) *Macro { return &Macro{Doink: t.Pos, Tokens: []*lexer.Token{t}} }
