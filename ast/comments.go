package ast

import (
	"strings"

	"gitlab.com/rigel314/go-verilog/lexer"
)

// A Comment node represents a single //-style or /*-style comment.
type Comment struct {
	Slash lexer.Pos // position of "/" starting the comment
	Text  string    // comment text (excluding '\n' for //-style comments)
}

func (c *Comment) Pos() lexer.Pos { return c.Slash }
func (c *Comment) End() lexer.Pos {
	offset := c.Slash.OffsetInLine
	lines := strings.Split(c.Text, "\n")
	if len(lines) == 1 {
		offset += len(lines[0])
	} else {
		offset = len(lines[len(lines)-1])
	}
	return lexer.Pos{
		Line:         c.Slash.Line + strings.Count(c.Text, "\n"),
		OffsetInLine: offset,
	}
}

func newComment(t *lexer.Token) *Comment { return &Comment{Slash: t.Pos, Text: t.Lit} }

// A CommentGroup represents a sequence of comments
// with no other tokens and no empty lines between.
type CommentGroup struct {
	List   []*Comment // len(List) > 0
	parent *CommentGroup
}

func (g *CommentGroup) Pos() lexer.Pos { return g.List[0].Pos() }
func (g *CommentGroup) End() lexer.Pos { return g.List[len(g.List)-1].End() }

func NewCommentGroup(parent *CommentGroup) *CommentGroup {
	return &CommentGroup{parent: parent}
}

func (cg *CommentGroup) Append(t *lexer.Token) {
	if cg.parent != nil {
		cg.parent.Append(t)
	}
	c := newComment(t)
	cg.List = append(cg.List, c)
}
