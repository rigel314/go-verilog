package ast

type SourceText struct {
	Doc          *CommentGroup
	Comments     []*CommentGroup
	Macros       []*Macro
	Timeunits    *TimeunitsDeclaration
	Descriptions []Description
}

func (sym *SourceText) String() (s string) {
	if sym.Timeunits != nil {
		s += sym.Timeunits.String() + "\n"
	}
	for _, v := range sym.Descriptions {
		s += v.String()
	}
	return
}

type TimeunitsDeclaration struct {
	FirstKeyword  string
	SecondKeyword string
	Unit          *TimeLiteral
	Precision     *TimeLiteral
}

func (sym *TimeunitsDeclaration) String() (s string) {
	s += sym.FirstKeyword + " "
	defer func() {
		s += ";"
	}()
	switch sym.FirstKeyword {
	case "timeunit":
		s += sym.Unit.String()
	case "timeprecision":
		s += sym.Precision.String()
	}
	if sym.SecondKeyword == "" && sym.Precision == nil {
		return
	}
	if sym.SecondKeyword == "" && sym.Precision != nil {
		s += " / " + sym.Precision.String()
		return
	}

	s += ";\n" + sym.SecondKeyword + " "
	switch sym.SecondKeyword {
	case "timeunit":
		s += sym.Unit.String()
	case "timeprecision":
		s += sym.Precision.String()
	}

	return
}

type TimeLiteral struct {
	Value *Fixed
	Unit  *TimeUnit
}

func (sym *TimeLiteral) String() (s string) {
	s += sym.Value.String() + sym.Unit.String()
	return
}

type Fixed struct {
	IntegerValue    string
	FractionalValue string
}

func (sym *Fixed) String() (s string) {
	if sym.FractionalValue != "" {
		s += sym.IntegerValue + "." + sym.FractionalValue
	} else {
		s += sym.IntegerValue
	}
	return
}

type Integer struct {
	Value string
}

func (sym *Integer) String() (s string) {
	s += sym.Value
	return
}

type TimeUnit struct {
	Unit string
}

func (sym *TimeUnit) String() (s string) {
	s += sym.Unit
	return
}

type Description interface {
	String() (s string)
	descNode() // descNode() ensures that only descriptions can be assigned to a Description
}

type ModuleDeclaration struct {
	Attributes    []*AttributeInstance
	Keyword       string
	Lifetime      *string
	Name          string
	PackageImport *PackageImportDeclaration
	Parameters    []*Parameter // nil means there was no # in the source, non-nil but empty means the source has an explicit empty parameter list eg "#()"
	Ports         []*Port
	Timeunits     *TimeunitsDeclaration
	ModuleItems   []*ModuleItem
	EndName       *string
}

func (*ModuleDeclaration) descNode() {}

func (sym *ModuleDeclaration) String() (s string) {
	s += "\n"
	return
}

type AttributeInstance struct {
}

func (sym *AttributeInstance) String() (s string) {
	s += "\n"
	return
}

type PackageImportDeclaration struct {
}

func (sym *PackageImportDeclaration) String() (s string) {
	s += "\n"
	return
}

type Parameter struct {
}

func (sym *Parameter) String() (s string) {
	s += "\n"
	return
}

type Port struct {
}

func (sym *Port) String() (s string) {
	s += "\n"
	return
}

type ModuleItem struct {
}

func (sym *ModuleItem) String() (s string) {
	s += "\n"
	return
}
