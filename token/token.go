package token

import (
	"strconv"
)

// Token holds the enum for the kinds of verilog tokens
type Token int

const (
	ILLEGAL Token = iota
	EOF
	COMMENT
	MACRO
	WHITESPACE

	literal_beg
	// Identifiers and basic type literals
	// (these tokens stand for classes of literals)
	IDENT
	UNBASEDUNSIZED
	NUMBER
	TICK
	STRING
	literal_end

	operator_beg
	DIV        // /
	DIV_ASSIGN // /=

	ASSIGN     // =
	EQUAL      // ==
	CASE_EQUAL // ===
	WILD_EQUAL // ==?

	LOGICAL_NOT    // !
	NOT_EQUAL      // !=
	CASE_NOT_EQUAL // !==
	WILD_NOT_EQUAL // !=?

	SUB        // -
	DEC        // --
	SUB_ASSIGN // -=
	IMP        // ->

	ADD        // +
	INC        // ++
	ADD_ASSIGN // +=

	MUL        // *
	POW        // **
	MUL_ASSIGN // *=
	RATTR      // *)

	QUESTION // ?

	COLON // :
	SCOPE // ::

	AND         // &
	LOGICAL_AND // &&
	AND_ASSIGN  // &=

	OR         // |
	LOGICAL_OR // ||
	OR_ASSIGN  // |=

	REM        // %
	REM_ASSIGN // %=

	XOR        // ^
	NOT_XOR    // ^~
	XOR_ASSIGN // ^=

	NOT         // ~
	NAND_REDUCE // ~&
	NOR_REDUCE  // ~|
	XNOR_REDUCE // ~^

	LESS                      // <
	LESS_EQUAL                // <=
	LOGICAL_SHIFT_LEFT        // <<
	LOGICAL_SHIFT_LEFT_ASSIGN // <<=
	SHIFT_LEFT                // <<<
	SHIFT_LEFT_ASSIGN         // <<<=
	EQUIVALENCE               // <->

	GREATER                    // >
	GREATER_EQUAL              // >=
	LOGICAL_SHIFT_RIGHT        // >>
	LOGICAL_SHIFT_RIGHT_ASSIGN // >>=
	SHIFT_RIGHT                // >>>
	SHIFT_RIGHT_ASSIGN         // >>>=

	LPAREN // (
	LATTR  // (*
	RPAREN // )

	LBRACKET  // [
	RBRACKET  // ]
	LBRACE    // {
	RBRACE    // }
	SEMICOLON // ;
	COMMA     // ,
	PERIOD    // .
	HASH      // #
	operator_end

	keyword_beg
	ACCEPT_ON
	ALIAS
	ALWAYS
	ALWAYS_COMB
	ALWAYS_FF
	ALWAYS_LATCH
	KWAND
	ASSERT
	KWASSIGN
	ASSUME
	AUTOMATIC
	BEFORE
	BEGIN
	BIND
	BINS
	BINSOF
	BIT
	BREAK
	BUF
	BUFIF0
	BUFIF1
	BYTE
	CASE
	CASEX
	CASEZ
	CELL
	CHANDLE
	CHECKER
	CLASS
	CLOCKING
	CMOS
	CONFIG
	CONST
	CONSTRAINT
	CONTEXT
	CONTINUE
	COVER
	COVERGROUP
	COVERPOINT
	CROSS
	DEASSIGN
	DEFAULT
	DEFPARAM
	DESIGN
	DISABLE
	DIST
	DO
	EDGE
	ELSE
	END
	ENDCASE
	ENDCHECKER
	ENDCLASS
	ENDCLOCKING
	ENDCONFIG
	ENDFUNCTION
	ENDGENERATE
	ENDGROUP
	ENDINTERFACE
	ENDMODULE
	ENDPACKAGE
	ENDPRIMITIVE
	ENDPROGRAM
	ENDPROPERTY
	ENDSPECIFY
	ENDSEQUENCE
	ENDTABLE
	ENDTASK
	ENUM
	EVENT
	EVENTUALLY
	EXPECT
	EXPORT
	EXTENDS
	EXTERN
	FINAL
	FIRST_MATCH
	FOR
	FORCE
	FOREACH
	FOREVER
	FORK
	FORKJOIN
	FUNCTION
	GENERATE
	GENVAR
	GLOBAL
	HIGHZ0
	HIGHZ1
	IF
	IFF
	IFNONE
	IGNORE_BINS
	ILLEGAL_BINS
	IMPLEMENTS
	IMPLIES
	IMPORT
	INCDIR
	INCLUDE
	INITIAL
	INOUT
	INPUT
	INSIDE
	INSTANCE
	INT
	INTEGER
	INTERCONNECT
	INTERFACE
	INTERSECT
	JOIN
	JOIN_ANY
	JOIN_NONE
	LARGE
	LET
	LIBLIST
	LIBRARY
	LOCAL
	LOCALPARAM
	LOGIC
	LONGINT
	MACROMODULE
	MATCHES
	MEDIUM
	MODPORT
	MODULE
	NAND
	NEGEDGE
	NETTYPE
	NEW
	NEXTTIME
	NMOS
	NOR
	NOSHOWCANCELLED
	KWNOT
	NOTIF0
	NOTIF1
	NULL
	KWOR
	OUTPUT
	PACKAGE
	PACKED
	PARAMETER
	PMOS
	POSEDGE
	PRIMITIVE
	PRIORITY
	PROGRAM
	PROPERTY
	PROTECTED
	PULL0
	PULL1
	PULLDOWN
	PULLUP
	PULSESTYLE_ONDETECT
	PULSESTYLE_ONEVENT
	PURE
	RAND
	RANDC
	RANDCASE
	RANDSEQUENCE
	RCMOS
	REAL
	REALTIME
	REF
	REG
	REJECT_ON
	RELEASE
	REPEAT
	RESTRICT
	RETURN
	RNMOS
	RPMOS
	RTRAN
	RTRANIF0
	RTRANIF1
	S_ALWAYS
	S_EVENTUALLY
	S_NEXTTIME
	S_UNTIL
	S_UNTIL_WITH
	SCALARED
	SEQUENCE
	SHORTINT
	SHORTREAL
	SHOWCANCELLED
	SIGNED
	SMALL
	SOFT
	SOLVE
	SPECIFY
	SPECPARAM
	STATIC
	KWSTRING
	STRONG
	STRONG0
	STRONG1
	STRUCT
	SUPER
	SUPPLY0
	SUPPLY1
	SYNC_ACCEPT_ON
	SYNC_REJECT_ON
	TABLE
	TAGGED
	TASK
	THIS
	THROUGHOUT
	TIME
	TIMEPRECISION
	TIMEUNIT
	TRAN
	TRANIF0
	TRANIF1
	TRI
	TRI0
	TRI1
	TRIAND
	TRIOR
	TRIREG
	TYPE
	TYPEDEF
	UNION
	UNIQUE
	UNIQUE0
	UNSIGNED
	UNTIL
	UNTIL_WITH
	UNTYPED
	USE
	UWIRE
	VAR
	VECTORED
	VIRTUAL
	VOID
	WAIT
	WAIT_ORDER
	WAND
	WEAK
	WEAK0
	WEAK1
	WHILE
	WILDCARD
	WIRE
	WITH
	WITHIN
	WOR
	XNOR
	KWXOR
	keyword_end
)

var tokens = [...]string{
	ILLEGAL:    "ILLEGAL",
	EOF:        "EOF",
	COMMENT:    "COMMENT",
	WHITESPACE: "WHITESPACE",

	IDENT:          "IDENT",
	UNBASEDUNSIZED: "UNBASEDUNSIZED",
	NUMBER:         "NUMBER",
	TICK:           "TICK",
	STRING:         "STRING",

	DIV:                        "/",
	DIV_ASSIGN:                 "/=",
	ASSIGN:                     "=",
	EQUAL:                      "==",
	CASE_EQUAL:                 "===",
	WILD_EQUAL:                 "==?",
	LOGICAL_NOT:                "!",
	NOT_EQUAL:                  "!=",
	CASE_NOT_EQUAL:             "!==",
	WILD_NOT_EQUAL:             "!=?",
	SUB:                        "-",
	DEC:                        "--",
	SUB_ASSIGN:                 "-=",
	IMP:                        "->",
	ADD:                        "+",
	INC:                        "++",
	ADD_ASSIGN:                 "+=",
	MUL:                        "*",
	POW:                        "**",
	MUL_ASSIGN:                 "*=",
	QUESTION:                   "?",
	COLON:                      ":",
	SCOPE:                      "::",
	AND:                        "&",
	LOGICAL_AND:                "&&",
	AND_ASSIGN:                 "&=",
	OR:                         "|",
	LOGICAL_OR:                 "||",
	OR_ASSIGN:                  "|=",
	REM:                        "%",
	REM_ASSIGN:                 "%=",
	XOR:                        "^",
	NOT_XOR:                    "^~",
	XOR_ASSIGN:                 "^=",
	NOT:                        "~",
	NAND_REDUCE:                "~&",
	NOR_REDUCE:                 "~|",
	XNOR_REDUCE:                "~^",
	LESS:                       "<",
	LESS_EQUAL:                 "<=",
	LOGICAL_SHIFT_LEFT:         "<<",
	LOGICAL_SHIFT_LEFT_ASSIGN:  "<<=",
	SHIFT_LEFT:                 "<<<",
	SHIFT_LEFT_ASSIGN:          "<<<=",
	EQUIVALENCE:                "<->",
	GREATER:                    "<",
	GREATER_EQUAL:              "<=",
	LOGICAL_SHIFT_RIGHT:        "<<",
	LOGICAL_SHIFT_RIGHT_ASSIGN: "<<=",
	SHIFT_RIGHT:                "<<<",
	SHIFT_RIGHT_ASSIGN:         "<<<=",
	LBRACKET:                   "[",
	RBRACKET:                   "]",
	LBRACE:                     "{",
	RBRACE:                     "}",
	LPAREN:                     "(",
	RPAREN:                     ")",
	SEMICOLON:                  ";",
	COMMA:                      ",",
	PERIOD:                     ".",

	ACCEPT_ON:           "accept_on",
	ALIAS:               "alias",
	ALWAYS:              "always",
	ALWAYS_COMB:         "always_comb",
	ALWAYS_FF:           "always_ff",
	ALWAYS_LATCH:        "always_latch",
	KWAND:               "and",
	ASSERT:              "assert",
	KWASSIGN:            "assign",
	ASSUME:              "assume",
	AUTOMATIC:           "automatic",
	BEFORE:              "before",
	BEGIN:               "begin",
	BIND:                "bind",
	BINS:                "bins",
	BINSOF:              "binsof",
	BIT:                 "bit",
	BREAK:               "break",
	BUF:                 "buf",
	BUFIF0:              "bufif0",
	BUFIF1:              "bufif1",
	BYTE:                "byte",
	CASE:                "case",
	CASEX:               "casex",
	CASEZ:               "casez",
	CELL:                "cell",
	CHANDLE:             "chandle",
	CHECKER:             "checker",
	CLASS:               "class",
	CLOCKING:            "clocking",
	CMOS:                "cmos",
	CONFIG:              "config",
	CONST:               "const",
	CONSTRAINT:          "constraint",
	CONTEXT:             "context",
	CONTINUE:            "continue",
	COVER:               "cover",
	COVERGROUP:          "covergroup",
	COVERPOINT:          "coverpoint",
	CROSS:               "cross",
	DEASSIGN:            "deassign",
	DEFAULT:             "default",
	DEFPARAM:            "defparam",
	DESIGN:              "design",
	DISABLE:             "disable",
	DIST:                "dist",
	DO:                  "do",
	EDGE:                "edge",
	ELSE:                "else",
	END:                 "end",
	ENDCASE:             "endcase",
	ENDCHECKER:          "endchecker",
	ENDCLASS:            "endclass",
	ENDCLOCKING:         "endclocking",
	ENDCONFIG:           "endconfig",
	ENDFUNCTION:         "endfunction",
	ENDGENERATE:         "endgenerate",
	ENDGROUP:            "endgroup",
	ENDINTERFACE:        "endinterface",
	ENDMODULE:           "endmodule",
	ENDPACKAGE:          "endpackage",
	ENDPRIMITIVE:        "endprimitive",
	ENDPROGRAM:          "endprogram",
	ENDPROPERTY:         "endproperty",
	ENDSPECIFY:          "endspecify",
	ENDSEQUENCE:         "endsequence",
	ENDTABLE:            "endtable",
	ENDTASK:             "endtask",
	ENUM:                "enum",
	EVENT:               "event",
	EVENTUALLY:          "eventually",
	EXPECT:              "expect",
	EXPORT:              "export",
	EXTENDS:             "extends",
	EXTERN:              "extern",
	FINAL:               "final",
	FIRST_MATCH:         "first_match",
	FOR:                 "for",
	FORCE:               "force",
	FOREACH:             "foreach",
	FOREVER:             "forever",
	FORK:                "fork",
	FORKJOIN:            "forkjoin",
	FUNCTION:            "function",
	GENERATE:            "generate",
	GENVAR:              "genvar",
	GLOBAL:              "global",
	HIGHZ0:              "highz0",
	HIGHZ1:              "highz1",
	IF:                  "if",
	IFF:                 "iff",
	IFNONE:              "ifnone",
	IGNORE_BINS:         "ignore_bins",
	ILLEGAL_BINS:        "illegal_bins",
	IMPLEMENTS:          "implements",
	IMPLIES:             "implies",
	IMPORT:              "import",
	INCDIR:              "incdir",
	INCLUDE:             "include",
	INITIAL:             "initial",
	INOUT:               "inout",
	INPUT:               "input",
	INSIDE:              "inside",
	INSTANCE:            "instance",
	INT:                 "int",
	INTEGER:             "integer",
	INTERCONNECT:        "interconnect",
	INTERFACE:           "interface",
	INTERSECT:           "intersect",
	JOIN:                "join",
	JOIN_ANY:            "join_any",
	JOIN_NONE:           "join_none",
	LARGE:               "large",
	LET:                 "let",
	LIBLIST:             "liblist",
	LIBRARY:             "library",
	LOCAL:               "local",
	LOCALPARAM:          "localparam",
	LOGIC:               "logic",
	LONGINT:             "longint",
	MACROMODULE:         "macromodule",
	MATCHES:             "matches",
	MEDIUM:              "medium",
	MODPORT:             "modport",
	MODULE:              "module",
	NAND:                "nand",
	NEGEDGE:             "negedge",
	NETTYPE:             "nettype",
	NEW:                 "new",
	NEXTTIME:            "nexttime",
	NMOS:                "nmos",
	NOR:                 "nor",
	NOSHOWCANCELLED:     "noshowcancelled",
	KWNOT:               "not",
	NOTIF0:              "notif0",
	NOTIF1:              "notif1",
	NULL:                "null",
	KWOR:                "or",
	OUTPUT:              "output",
	PACKAGE:             "package",
	PACKED:              "packed",
	PARAMETER:           "parameter",
	PMOS:                "pmos",
	POSEDGE:             "posedge",
	PRIMITIVE:           "primitive",
	PRIORITY:            "priority",
	PROGRAM:             "program",
	PROPERTY:            "property",
	PROTECTED:           "protected",
	PULL0:               "pull0",
	PULL1:               "pull1",
	PULLDOWN:            "pulldown",
	PULLUP:              "pullup",
	PULSESTYLE_ONDETECT: "pulsestyle_ondetect",
	PULSESTYLE_ONEVENT:  "pulsestyle_onevent",
	PURE:                "pure",
	RAND:                "rand",
	RANDC:               "randc",
	RANDCASE:            "randcase",
	RANDSEQUENCE:        "randsequence",
	RCMOS:               "rcmos",
	REAL:                "real",
	REALTIME:            "realtime",
	REF:                 "ref",
	REG:                 "reg",
	REJECT_ON:           "reject_on",
	RELEASE:             "release",
	REPEAT:              "repeat",
	RESTRICT:            "restrict",
	RETURN:              "return",
	RNMOS:               "rnmos",
	RPMOS:               "rpmos",
	RTRAN:               "rtran",
	RTRANIF0:            "rtranif0",
	RTRANIF1:            "rtranif1",
	S_ALWAYS:            "s_always",
	S_EVENTUALLY:        "s_eventually",
	S_NEXTTIME:          "s_nexttime",
	S_UNTIL:             "s_until",
	S_UNTIL_WITH:        "s_until_with",
	SCALARED:            "scalared",
	SEQUENCE:            "sequence",
	SHORTINT:            "shortint",
	SHORTREAL:           "shortreal",
	SHOWCANCELLED:       "showcancelled",
	SIGNED:              "signed",
	SMALL:               "small",
	SOFT:                "soft",
	SOLVE:               "solve",
	SPECIFY:             "specify",
	SPECPARAM:           "specparam",
	STATIC:              "static",
	KWSTRING:            "string",
	STRONG:              "strong",
	STRONG0:             "strong0",
	STRONG1:             "strong1",
	STRUCT:              "struct",
	SUPER:               "super",
	SUPPLY0:             "supply0",
	SUPPLY1:             "supply1",
	SYNC_ACCEPT_ON:      "sync_accept_on",
	SYNC_REJECT_ON:      "sync_reject_on",
	TABLE:               "table",
	TAGGED:              "tagged",
	TASK:                "task",
	THIS:                "this",
	THROUGHOUT:          "throughout",
	TIME:                "time",
	TIMEPRECISION:       "timeprecision",
	TIMEUNIT:            "timeunit",
	TRAN:                "tran",
	TRANIF0:             "tranif0",
	TRANIF1:             "tranif1",
	TRI:                 "tri",
	TRI0:                "tri0",
	TRI1:                "tri1",
	TRIAND:              "triand",
	TRIOR:               "trior",
	TRIREG:              "trireg",
	TYPE:                "type",
	TYPEDEF:             "typedef",
	UNION:               "union",
	UNIQUE:              "unique",
	UNIQUE0:             "unique0",
	UNSIGNED:            "unsigned",
	UNTIL:               "until",
	UNTIL_WITH:          "until_with",
	UNTYPED:             "untyped",
	USE:                 "use",
	UWIRE:               "uwire",
	VAR:                 "var",
	VECTORED:            "vectored",
	VIRTUAL:             "virtual",
	VOID:                "void",
	WAIT:                "wait",
	WAIT_ORDER:          "wait_order",
	WAND:                "wand",
	WEAK:                "weak",
	WEAK0:               "weak0",
	WEAK1:               "weak1",
	WHILE:               "while",
	WILDCARD:            "wildcard",
	WIRE:                "wire",
	WITH:                "with",
	WITHIN:              "within",
	WOR:                 "wor",
	XNOR:                "xnor",
	KWXOR:               "xor",
}

// String returns the string corresponding to the token tok.
// For operators, delimiters, and keywords the string is the actual
// token character sequence (e.g., for the token ADD, the string is
// "+"). For all other tokens the string corresponds to the token
// constant name (e.g. for the token IDENT, the string is "IDENT").
//
func (tok Token) String() string {
	s := ""
	if 0 <= tok && tok < Token(len(tokens)) {
		s = tokens[tok]
	}
	if s == "" {
		s = "token(" + strconv.Itoa(int(tok)) + ")"
	}
	return s
}

var keywords map[string]Token

func init() {
	keywords = make(map[string]Token)
	for i := keyword_beg + 1; i < keyword_end; i++ {
		keywords[tokens[i]] = i
	}
}

// Lookup maps an identifier to its keyword token or IDENT (if not a keyword).
func Lookup(ident string) Token {
	if tok, is_keyword := keywords[ident]; is_keyword {
		return tok
	}
	return IDENT
}

// IsLiteral returns true for tokens corresponding to identifiers
// and basic type literals; it returns false otherwise.
//
func (tok Token) IsLiteral() bool { return literal_beg < tok && tok < literal_end }

// IsOperator returns true for tokens corresponding to operators and
// delimiters; it returns false otherwise.
//
func (tok Token) IsOperator() bool { return operator_beg < tok && tok < operator_end }

// IsKeyword returns true for tokens corresponding to keywords;
// it returns false otherwise.
//
func (tok Token) IsKeyword() bool { return keyword_beg < tok && tok < keyword_end }
