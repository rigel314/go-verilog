package parser

import (
	"fmt"
	"io"

	"gitlab.com/rigel314/go-verilog/ast"
	"gitlab.com/rigel314/go-verilog/lexer"
	"gitlab.com/rigel314/go-verilog/token"
)

var ErrUnexpectedToken = fmt.Errorf("invalid token")
var ErrMissingSemicolon = fmt.Errorf("%w: missing semicolon", ErrUnexpectedToken)
var ErrUnsupportedToken = fmt.Errorf("%w: not yet supported", ErrUnexpectedToken)

type Parser struct {
	l                 *lexer.Lexer
	last              *lexer.Token
	lastNonWhitespace token.Token
	lastValid         bool
	comments          []*ast.CommentGroup
	macros            []*ast.Macro
}

func NewParser(filename string, r io.Reader) *Parser {
	return &Parser{
		l: lexer.NewLexer(filename, r),
	}
}

func (p *Parser) ParseSV() (*ast.SourceText, error) {
	return p.source_text()
}

func (p *Parser) lex() *lexer.Token {
	if p.lastValid {
		p.lastValid = false
		return p.last
	}
	p.last = p.l.Lex()
	return p.last
}

func (p *Parser) unlex() {
	if p.lastValid {
		panic("double unlex")
	}
	if p.last == nil {
		panic("unlex without read")
	}
	p.lastValid = true
}

func (p *Parser) lexIgnoreWhitespace() *lexer.Token {
	tok := p.lex()
	if tok.Typ == token.WHITESPACE {
		tok = p.lex()
	}
	return tok
}

func (p *Parser) lexConsumeComments() *lexer.Token {
	for {
		tok := p.lexIgnoreWhitespace()
		if tok.Typ == token.COMMENT {
			// append comment group heirachy
			if p.lastNonWhitespace == token.COMMENT && len(p.comments) > 0 {
				lastcommentend := p.comments[len(p.comments)-1].End().Line
				if lastcommentend == tok.Pos.Line || lastcommentend == tok.Pos.Line-1 {
					p.comments[len(p.comments)-1].Append(tok)

					p.lastNonWhitespace = tok.Typ
					continue
				}
			}
			cg := ast.NewCommentGroup(nil)
			cg.Append(tok)
			p.comments = append(p.comments, cg)

			p.lastNonWhitespace = tok.Typ
			continue
		}
	}
}

func (p *Parser) next() *lexer.Token {
	for {
		tok := p.lexIgnoreWhitespace()
		if tok.Typ == token.MACRO {
			// parse macro
			// append macro to macro list
			p.lastNonWhitespace = tok.Typ
			continue
		}

		p.lastNonWhitespace = tok.Typ
		return tok
	}
}
