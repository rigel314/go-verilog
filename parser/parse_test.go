package parser

import (
	"bytes"
	"reflect"
	"testing"

	"gitlab.com/rigel314/go-verilog/ast"
)

func TestTimeunits_declaration(t *testing.T) {
	timeunits := []struct {
		code        string
		expectNoErr bool
		val         *ast.TimeunitsDeclaration
	}{
		{"timeunit 1ns;", true, &ast.TimeunitsDeclaration{FirstKeyword: "timeunit", SecondKeyword: "", Unit: NewParser("", bytes.NewBufferString("1ns")).must_time_literal(), Precision: nil}},
		{"timeunit 1ns/1ps;", true, &ast.TimeunitsDeclaration{FirstKeyword: "timeunit", SecondKeyword: "", Unit: NewParser("", bytes.NewBufferString("1ns")).must_time_literal(), Precision: NewParser("", bytes.NewBufferString("1ps")).must_time_literal()}},
		{"timeunit 1ns; timeprecision 1ps;", true, &ast.TimeunitsDeclaration{FirstKeyword: "timeunit", SecondKeyword: "timeprecision", Unit: NewParser("", bytes.NewBufferString("1ns")).must_time_literal(), Precision: NewParser("", bytes.NewBufferString("1ps")).must_time_literal()}},
		{"timeprecision 1ps;", true, &ast.TimeunitsDeclaration{FirstKeyword: "timeprecision", SecondKeyword: "", Unit: nil, Precision: NewParser("", bytes.NewBufferString("1ps")).must_time_literal()}},
		{"timeprecision 1ps; timeunit 1ns;", true, &ast.TimeunitsDeclaration{FirstKeyword: "timeprecision", SecondKeyword: "timeunit", Unit: NewParser("", bytes.NewBufferString("1ns")).must_time_literal(), Precision: NewParser("", bytes.NewBufferString("1ps")).must_time_literal()}},
		{"timeunit;", false, nil},
		{"timeunit 1ns/;", false, nil},
		{"timeunit 1ns", false, nil},
		{"timeunit 1ns/1ps", false, nil},
		{"timeunit 1ns; timeprecision 1ps", false, nil},
		{"timeprecision 1ps", false, nil},
		{"timeprecision 1ps; timeunit 1ns", false, nil},
	}
	for _, v := range timeunits {
		p := NewParser("", bytes.NewBuffer(([]byte)(v.code)))

		parse, err := p.timeunits_declaration()
		if (v.expectNoErr && err != nil) ||
			(!v.expectNoErr && err == nil) {
			t.Log("error parsing '", v, "': ", err)
			t.Fail()
		}
		if !reflect.DeepEqual(v.val, parse) {
			t.Log("invalid parse on '", v)
			t.Log("got", parse, "expected", v.val)
			t.Fail()
		}
	}
}
