package parser

import (
	"bytes"
	"reflect"
	"testing"
)

func TestUnlex(t *testing.T) {
	p := NewParser("", bytes.NewBuffer(([]byte)("a b c")))

	x1 := p.lex()
	t.Log(x1)
	p.unlex()
	x2 := p.lex()
	t.Log(x2)
	p.unlex()
	x3 := p.lex()
	t.Log(x3)

	if !reflect.DeepEqual(x1, x2) {
		t.Fail()
	}
	if !reflect.DeepEqual(x2, x3) {
		t.Fail()
	}
}
