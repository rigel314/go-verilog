package parser

import (
	"fmt"

	"gitlab.com/rigel314/go-verilog/ast"
	"gitlab.com/rigel314/go-verilog/lexer"
	"gitlab.com/rigel314/go-verilog/token"
)

var errCanContinue = fmt.Errorf("%w", ErrUnexpectedToken)

func (p *Parser) source_text() (*ast.SourceText, error) {
	ret := ast.SourceText{}
	tok := p.next()
	switch tok.Typ {
	case token.TIMEUNIT, token.TIMEPRECISION:
		p.unlex()
		td, err := p.timeunits_declaration()
		if err != nil {
			return nil, err
		}
		ret.Timeunits = td
	default:
		p.unlex()
	}

	for {
		d, err := p.description()
		if err != nil {
			return nil, err
		}
		if d == nil {
			break
		}
		ret.Descriptions = append(ret.Descriptions, d)
	}
	return &ret, nil
}

func (p *Parser) timeunits_declaration() (*ast.TimeunitsDeclaration, error) {
	ret := ast.TimeunitsDeclaration{}
	tok := p.next()
	if !tok.Typ.IsKeyword() {
		p.unlex()
		return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
	}
	switch tok.Lit {
	case "timeunit":
		ret.FirstKeyword = tok.Lit
		top, err := p.time_literal()
		if err != nil {
			return nil, err
		}
		ret.Unit = top

		slash := p.next()
		if !slash.Typ.IsOperator() {
			p.unlex()
			return nil, fmt.Errorf("%v: %w", slash, ErrUnexpectedToken)
		}
		switch slash.Lit {
		case "/":
			bottom, err := p.time_literal()
			if err != nil {
				return nil, err
			}
			ret.Precision = bottom
			semi := p.next()
			if semi.Typ != token.SEMICOLON {
				p.unlex()
				return nil, fmt.Errorf("%v: %w", semi, ErrUnexpectedToken)
			}
		case ";":
			prec := p.next()
			if prec.Typ != token.TIMEPRECISION {
				p.unlex()
				return &ret, nil
			}
			ret.SecondKeyword = prec.Lit
			bottom, err := p.time_literal()
			if err != nil {
				return nil, err
			}
			ret.Precision = bottom
			semi := p.next()
			if semi.Typ != token.SEMICOLON {
				p.unlex()
				return nil, fmt.Errorf("%v: %w", semi, ErrUnexpectedToken)
			}
		default:
			p.unlex()
			return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
		}
	case "timeprecision":
		ret.FirstKeyword = tok.Lit
		prec, err := p.time_literal()
		if err != nil {
			return nil, err
		}
		ret.Precision = prec
		semi := p.next()
		if semi.Typ != token.SEMICOLON {
			p.unlex()
			return nil, fmt.Errorf("%v: %w", semi, ErrUnexpectedToken)
		}
		unit := p.next()
		if unit.Typ != token.TIMEUNIT {
			p.unlex()
			return &ret, nil
		}
		ret.SecondKeyword = unit.Lit
		u, err := p.time_literal()
		if err != nil {
			return nil, err
		}
		ret.Unit = u
		semi2 := p.next()
		if semi2.Typ != token.SEMICOLON {
			p.unlex()
			return nil, fmt.Errorf("%v: %w", semi2, ErrUnexpectedToken)
		}
	default:
		p.unlex()
		return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
	}
	return &ret, nil
}

func (p *Parser) time_literal() (*ast.TimeLiteral, error) {
	ret := ast.TimeLiteral{}

	uf, err := p.unsigned_number_OR_fixed_point_number()
	if err != nil {
		return nil, err
	}
	ret.Value = uf.Fixed

	tu, err := p.time_unit()
	if err != nil {
		return nil, err
	}
	ret.Unit = tu

	return &ret, nil
}

type unsignedOrFixed struct {
	*ast.Fixed
	isFixed bool
}

func (p *Parser) unsigned_number_OR_fixed_point_number() (*unsignedOrFixed, error) {
	ret := unsignedOrFixed{Fixed: new(ast.Fixed)}

	tok := p.next()
	if tok.Typ != token.NUMBER {
		p.unlex()
		return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
	}
	ret.IntegerValue = tok.Lit

	tok = p.lex()

	// BUG(rigel314): whitespace is not allowed between the number and the '.' or between the '.' and the number, but comments and macros probably can be
	if tok.Typ != token.PERIOD {
		p.unlex()
		return &ret, nil
	}

	ret.isFixed = true

	tok = p.lex()
	if tok.Typ != token.NUMBER {
		p.unlex()
		return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
	}
	ret.FractionalValue = tok.Lit

	return &ret, nil
}

func (p *Parser) time_unit() (*ast.TimeUnit, error) {
	ret := ast.TimeUnit{}

	tok := p.lex()
	if tok.Typ != token.IDENT {
		p.unlex()
		return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
	}
	switch tok.Lit {
	case "s", "ms", "us", "ns", "ps", "fs":
		ret.Unit = tok.Lit
	default:
		p.unlex()
		return nil, fmt.Errorf("%v: %w", tok, ErrUnexpectedToken)
	}

	return &ret, nil
}

func (p *Parser) description() (ast.Description, error) {
	// OR (module_declaration, udp_declaration, interface_declaration, program_declaration, package_declaration, config_declaration, {attribute_instance}package_item, {attribute_instance}bind_directive)

	var attributes []*ast.AttributeInstance

	var tok *lexer.Token

	for {
		tok = p.next()

		if tok.Typ != token.LATTR {
			break
		}
		p.unlex()

		attr, err := p.attribute_instance()
		if err != nil {
			return nil, err
		}
		attributes = append(attributes, attr)
	}

	switch tok.Typ {
	case token.EOF:
		return nil, nil
	case token.MODULE, token.MACROMODULE:
		return p.module_declaration(attributes, tok)
	case token.INTERFACE:
		return nil, ErrUnsupportedToken
	case token.PACKAGE:
		return nil, ErrUnsupportedToken
	}

	return nil, ErrUnexpectedToken
}

func (p *Parser) attribute_instance() (*ast.AttributeInstance, error) {
	return nil, nil
}

func (p *Parser) module_declaration(attributes []*ast.AttributeInstance, kw *lexer.Token) (*ast.ModuleDeclaration, error) {
	ret := ast.ModuleDeclaration{}

	ret.Attributes = attributes
	ret.Keyword = kw.Lit

	tok := p.next()

	switch tok.Typ {
	case token.STATIC, token.AUTOMATIC:
		ret.Lifetime = &tok.Lit

		tok := p.next()
		if tok.Typ != token.IDENT {
			return nil, ErrUnexpectedToken
		}
		ret.Name = tok.Lit
	case token.IDENT:
		ret.Name = tok.Lit
	default:
		return nil, ErrUnexpectedToken
	}

	tok = p.next()
	if tok.Typ == token.IMPORT {
		p.unlex()
		importDec, err := p.package_import_declaration()
		if err != nil {
			return nil, err
		}
		ret.PackageImport = importDec
		tok = p.next()
	}

	if tok.Typ == token.HASH {
		t := p.next()
		if t.Typ == token.LPAREN {
			t := p.next()
			if t.Typ == token.RPAREN { // explicit empty param list eg "#()"
				ret.Parameters = []*ast.Parameter{}
			} else {
				p.unlex()
			module_declaration_paramLoop:
				for {
					param, err := p.parameter()
					if err != nil {
						return nil, err
					}
					ret.Parameters = append(ret.Parameters, param)
					t := p.next()
					switch t.Typ {
					case token.COMMA:
						continue
					case token.RPAREN:
						tok = p.next()
						break module_declaration_paramLoop
					default:
						return nil, ErrUnexpectedToken
					}
				}
			}
		}
	}

	if tok.Typ != token.RPAREN {
		return nil, ErrUnexpectedToken
	}

module_declaration_portLoop:
	for {
		port, err := p.port()
		if err != nil {
			return nil, err
		}
		ret.Ports = append(ret.Ports, port)
		tok := p.next()
		switch tok.Typ {
		case token.COMMA:
			continue
		case token.RPAREN:
			tok = p.next()
			break module_declaration_portLoop
		default:
			return nil, ErrUnexpectedToken
		}
	}

	tok = p.next()
	if tok.Typ != token.SEMICOLON {
		return nil, ErrUnexpectedToken
	}

	// Header is done now, lets parse the contents

	tok = p.next()

	if tok.Typ == token.TIMEUNIT || tok.Typ == token.TIMEPRECISION {
		p.unlex()
		td, err := p.timeunits_declaration()
		if err != nil {
			return nil, err
		}
		ret.Timeunits = td
	}

	if tok.Typ != token.ENDMODULE {
	module_declaration_itemLoop:
		for {
			item, err := p.module_item()
			if err != nil {
				return nil, err
			}
			ret.ModuleItems = append(ret.ModuleItems, item)

			tok = p.next()
			if tok.Typ == token.ENDMODULE {
				break module_declaration_itemLoop
			}
			p.unlex()
		}
	}

	// endmodule found

	tok = p.next()
	if tok.Typ != token.COLON {
		p.unlex()
		return &ret, nil
	}

	tok = p.next()
	if tok.Typ != token.IDENT {
		return nil, ErrUnexpectedToken
	}

	ret.EndName = &tok.Lit

	return &ret, nil
}

func (p *Parser) package_import_declaration() (*ast.PackageImportDeclaration, error) {
	return nil, nil
}

func (p *Parser) parameter() (*ast.Parameter, error) {
	return nil, nil
}

func (p *Parser) port() (*ast.Port, error) {
	return nil, nil
}

func (p *Parser) module_item() (*ast.ModuleItem, error) {
	return nil, nil
}

func (p *Parser) macro() (*ast.Macro, error) {

	return nil, nil
}
