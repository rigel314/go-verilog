package parser

import "gitlab.com/rigel314/go-verilog/ast"

func (p *Parser) must_time_literal() *ast.TimeLiteral {
	ret, err := p.time_literal()
	if err != nil {
		panic(err.Error())
	}
	return ret
}
